# Quadtree Image Filter

This is a small CLI tool to apply a Quadtree style image filter to images. Quadtree filtering works by looking at the whole image and if the difference between the value of all the pixels is above a certain threshold it will split the image into 4 equal squares. It will then look at each of those squares, check if the difference between the pixels is above the threshold and then either split the region again or fill it with the average value of the square. This is repeated recursively until all the squares have been filled with a average value. 

My implementation splits the image into its 3 color channels (and alpha if its a png), and performs the Quadtree filtering on each color layer individually and then adds the filtered layers back together for a full color image. This implementation gives the image a bit of a distorted look compared to applying the Quadtree filtering on all the layers at the time.

## Installation

This project only depends on numpy and pillow to work. You can either install them individually or simply run
``` 
pip install -r requirements.txt
```

## Usage

To use this tool simply call it through the command line in your terminal like this

```
python main.py -i ../the/path/to/your/image.image_extension
```

Additionally there are two flags you can add. 

The first is the "-d" flag which dictates the detail of the image. The default value for this flag is 30. A lower value will give you a more detailed image i.e more subdivisions while a higher value will give you a less detailed image. Take note that a more detailed image takes longer to process in this implementation.

```
python main.py -i ../the/path/to/your/image.image_extension -d 70
```

The second flag is the "-l" flag which allows you to export each color layer as a separate image. This defaults to false but in case you want all the layers simply run the tool like this:

```
python main.py -i ../the/path/to/your/image.image_extension -l True
```

Of course you can use both of the flags at the same time like this

```
python main.py -i ../the/path/to/your/image.image_extension -d 35 -l True
```

## Examples

![RGB Gradient](./example_images/transparent_gradient.png){width=250px height=250px}
*Original image*

![RGB Gradient Red channel processed](./example_images/transparent_gradient_quadtree_R.png){width=250px height=250px}
*Red channel processed*

![RGB Gradient Green channel processed](./example_images/transparent_gradient_quadtree_G.png){width=250px height=250px}
*Green channel processed*

![RGB Gradient Blue channel processed](./example_images/transparent_gradient_quadtree_B.png){width=250px height=250px}
*Blue channel processed*

![RGB Gradient Alpha channel processed](./example_images/transparent_gradient_quadtree_A.png){width=250px height=250px}
*Alpha channel processed*

![RGB Gradient Processed](./example_images/transparent_gradient_quadtree_RGB.png){width=250px height=250px}
*All processed channels combined*

---

![Shiny Egg](./example_images/example_image.jpg){width=250px height=250px}
*Original image of a shiny egg*

![Shiny Egg Processed](./example_images/example_image_quadtree_RGB.jpg){width=250px height=250px}
*Processed image of a shiny egg*
