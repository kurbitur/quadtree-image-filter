import argparse
import os
import numpy as np
from PIL import Image, ImageOps


def closest_power_of_2(K, IMAGE_SIZES):
    closest = min(IMAGE_SIZES, key=lambda x: abs(x - K))
    return (closest, closest)


def process_image_array(img_array, threshold=30):
    if img_array.std() > threshold:
        # if the value of the std is greater than something then split
        # [0,1]
        # [2,3] is the order of the arrays
        half_veritcal = np.vsplit(img_array, 2)
        squares = [np.hsplit(half, 2) for half in half_veritcal]
        squares = [item for sublist in squares for item in sublist]
        new_squares = [process_image_array(square, threshold) for square in squares]
        return np.block(
            [[new_squares[0], new_squares[1]], [new_squares[2], new_squares[3]]]
        )
    else:
        # Create a new array with the same shape as the img_array
        # and fill it with the average/mean value of the img_array
        return np.full(np.shape(img_array), int(img_array.mean()))
        # TODO:Would be cool to add a way to make a border around it -> basically just make first and last column value be 0 or 255


def main():
    IMAGE_SIZES = [
        1,
        2,
        4,
        8,
        16,
        32,
        64,
        128,
        256,
        512,
        1024,
        2048,
    ]  # This could a bigger list but processing bigger images takes longer
    parser = argparse.ArgumentParser("Quadtree image processing filter")
    parser.add_argument("-i", metavar="INPUT", type=str, help="Input file")
    parser.add_argument(
        "-d",
        metavar="DETAIL",
        default=30,
        type=int,
        help="How detailed do you want the image to be, lower value means more detail. Number has to be in range 1 - 255",
    )
    parser.add_argument(
        "-l",
        metavar="LAYERS",
        default=False,
        type=bool,
        help="Change this to true if you want to export every color layer of the image to a seperate file",
    )

    args = parser.parse_args()
    abs_path = os.path.abspath(args.i)
    filename, file_extension = os.path.splitext(os.path.basename(args.i))

    # Check if file exists
    if not os.path.isfile(abs_path):
        print(f"No such file: '{abs_path}'")
        return

    # Try to open the file as an image
    try:
        image = Image.open(abs_path)
    except IOError:
        print(f"Cannot open image file: '{abs_path}'")
        return

    # Process the image so it is square and of the right size.
    width, height = image.size
    if width != height:
        # Crop the image into a square
        print("Cropping image into a square")
        smaller_size = min(width, height)
        image = ImageOps.fit(image, (smaller_size, smaller_size))

    if image.size[0] not in IMAGE_SIZES:
        # Resize the image
        print("Resizing image")
        desired_size = closest_power_of_2(image.size[0], IMAGE_SIZES)
        image = image.resize(desired_size, Image.Resampling.LANCZOS)

    # Split the image into layers, apply the quadtree filter to each layer individually and then merge them together
    # TODO: This errors on a 1 layer/black and white image.
    print("Processing Image")
    LAYER_NAMES = ["R", "G", "B", "A"]
    image_layers = image.split()
    processed_image_layers = []
    for index, layer in enumerate(image_layers):
        print(f"Processing {LAYER_NAMES[index]} layer")
        image_as_array = np.array(layer)
        quadtree_layer = process_image_array(image_as_array, args.d)
        processed_image_layers.append(quadtree_layer)
        if args.l:
            layer_image = Image.fromarray(quadtree_layer.astype(np.uint8))
            layer_image.save(
                f"{filename}_quadtree_{LAYER_NAMES[index]}{file_extension}"
            )

    # Create RGB(A) Image
    print("Combining layers and exporting image")
    color_image = Image.fromarray(np.dstack(processed_image_layers).astype(np.uint8))
    # TODO: Export to a prooper output folder
    color_image.save(f"{filename}_quadtree_RGB{file_extension}")


if __name__ == "__main__":
    main()
